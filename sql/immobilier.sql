-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 04 juin 2024 à 08:34
-- Version du serveur : 8.2.0
-- Version de PHP : 8.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `immobilier`
--

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

DROP TABLE IF EXISTS `contact`;
CREATE TABLE IF NOT EXISTS `contact` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vos_demandes` longtext COLLATE utf8mb4_unicode_ci,
  `disponibilites` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `contact`
--

INSERT INTO `contact` (`id`, `nom`, `prenom`, `email`, `tel`, `vos_demandes`, `disponibilites`) VALUES
(16, 'nico', 'poiret', 'nicopatch@hotmail.fr', '0645261728', 'payer moins d\'impots', '2024-05-30 13:45:00'),
(17, 'lignac', 'cyril', 'cl@gmail.com', '0728273647', 'investir dans la pierre', '2024-06-04 00:43:00'),
(18, 'franki', 'razakanaivo', 'razak@gmail.fr', '0618000024', 'gagner plus d\'argent en investissant dans des placements longs termes', '2024-05-31 15:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8mb3_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20240408140424', '2024-04-09 11:28:39', 207),
('DoctrineMigrations\\Version20240425124436', '2024-04-25 14:45:01', 754),
('DoctrineMigrations\\Version20240425222702', '2024-04-26 00:28:21', 182);

-- --------------------------------------------------------

--
-- Structure de la table `formulaire_recherche`
--

DROP TABLE IF EXISTS `formulaire_recherche`;
CREATE TABLE IF NOT EXISTS `formulaire_recherche` (
  `id` int NOT NULL AUTO_INCREMENT,
  `departement` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surface_min` int DEFAULT NULL,
  `prix_min` int DEFAULT NULL,
  `prix_max` int DEFAULT NULL,
  `loyer_min` int DEFAULT NULL,
  `loyer_max` int DEFAULT NULL,
  `rentabilite` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `habitation`
--

DROP TABLE IF EXISTS `habitation`;
CREATE TABLE IF NOT EXISTS `habitation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ville` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adresse` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_postal` int NOT NULL,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surface` int NOT NULL,
  `prix` int NOT NULL,
  `loyer` int NOT NULL,
  `rentabilite` int NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_creation` date NOT NULL,
  `departement` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `habitation`
--

INSERT INTO `habitation` (`id`, `ville`, `adresse`, `code_postal`, `type`, `surface`, `prix`, `loyer`, `rentabilite`, `image`, `date_creation`, `departement`, `description`) VALUES
(18, 'paris', '5 rue du maréchal', 75001, 'Duplex', 115, 3000000, 4000, 12, 'living-room-661e718e8026c.jpg', '2024-04-16', '75 - Paris - Paris', ''),
(19, 'guyancourt', '10 mail pierre theilard de chardin', 78280, 'Appartement', 80, 380000, 900, 10, 'sweet-home-661e6a2d1184a.jpg', '2024-04-16', '78 - Yvelines - Versailles', ''),
(20, 'mulhouse', '23 rue mathias grunewald', 68200, 'Appartement', 90, 280000, 780, 8, 'living-room-gris-661e6a7e840e3.jpg', '2024-04-16', '68 - Haut-Rhin - Colmar', ''),
(21, 'nice', '8 rue des promeneurs anglais', 6340, 'Villa', 180, 490000, 1200, 11, 'vue-661e6afe88119.jpg', '2024-04-15', '06 - Alpes Maritimes - Nice', ''),
(22, 'strasbourg', '3 avenue des bouchers', 68400, 'Maison', 130, 380000, 1190, 10, 'cuisine-marron-661e7003c5de2.jpg', '2024-04-11', '67 - Bas-Rhin - Strasbourg', ''),
(23, 'dijon', '19 rue des cigognes', 31760, 'Maison', 180, 500000, 2790, 18, 'dining-room-662134f03af2d.jpg', '2024-04-16', '21 - Côte-d\'Or - Dijon', ''),
(24, 'Rodez', '10 rue des gangsters', 12900, 'Appartement', 120, 298000, 890, 9, 'bar-662133f9a94a9.jpg', '2024-04-03', '12 - Aveyron - Rodez', ''),
(26, 'rennes', '98 avenue des champs fleuris', 35670, 'Appartement', 129, 370000, 870, 17, 'francesca-tosolini-tHkJAMcO3QE-unsplash-66223adecffeb.jpg', '2024-04-18', '35 - Ille-et-Vilaine - Rennes', 'Hyper spacieux'),
(27, 'avignon', '7 rue du pont', 84300, 'Appartement', 78, 230000, 780, 13, 'deborah-cortelazzi-gREquCUXQLI-unsplash-66213376474dd.jpg', '2024-04-18', '84 - Vaucluse - Avignon', ''),
(28, 'angoulême', '4 rue des agneaux', 17650, 'Appartement', 135, 330000, 930, 14, 'frames-for-your-heart-FqqiAvJejto-unsplash-662228ec98cd6.jpg', '2024-04-13', '16 - Charente - Angoulême', 'magnifique appartement spacieux\r\n- beaucoup de rangement\r\n- proche du bus 320'),
(29, 'gap', '9 rue des asperges', 5430, 'Maison', 110, 310000, 1000, 10, 'kitchen-2165756_1280-66222836e6dd3.jpg', '2024-04-19', '05 - Hautes-Alpes - Gap', 'Sublime maison, proche des commodités, 8 mn des supermarchés, 3mn des écoles, proche de la nature.\r\nIdéal pour une famille avec des enfants!'),
(30, 'clermont-ferrand', '2 rue de la source', 63200, 'Maison', 128, 400000, 980, 14, 'kitchenette-66223a43948a2.jpg', '2024-04-04', '45 - Loiret - Orléans', 'Appartement exceptionnel avec vue sur les montagnes'),
(31, 'guyancourt', '9 rue des fleurs', 78280, 'Appartement', 130, 240000, 980, 8, 'appart-6622315627359.jpg', '2024-04-12', '03 - Allier - Moulins', 'au top!'),
(32, 'guyancourt', '9 rue des fleurs', 68200, 'Duplex', 100, 490000, 900, 8, 'kitchen-66223a7c6b4e7.jpg', '2024-04-03', '10 - Aube - Troyes', 'splendide'),
(33, 'Foix', '23 rue mathias grunewald', 68400, 'Maison', 80, 240000, 900, 10, 'deborah-cortelazzi-gREquCUXQLI-unsplash-66325209821b6.jpg', '2024-04-11', '09 - Ariège - Foix', 'beaux volumes'),
(36, 'biarritz', 'rue des surfers', 33350, 'Maison', 150, 600000, 1200, 14, 'interior-1961070_1280-66224f28d3739.jpg', '2024-04-17', '33 - Gironde - Bordeaux', NULL),
(37, 'cergy', '8 rue des acacias', 95800, 'Duplex', 130, 490000, 1200, 10, 'bedroom-66262aa22ea73.jpg', '2024-04-22', '95 - Val-D\'Oise - Pontoise', 'Superbe duplex grands espaces'),
(38, 'auvers', '30 RUE NNN', 95430, 'Appartement', 100, 280000, 780, 9, 'deborah-cortelazzi-gREquCUXQLI-unsplash-662641a827e2d.jpg', '2024-04-15', '95 - Val-D\'Oise - Pontoise', NULL),
(39, 'juans les pins', '5 rue du maréchal', 33400, 'Appartement', 123, 345000, 1200, 13, 'elegant-6647387ac9c62.jpg', '2024-05-23', '33 - Gironde - Bordeaux', 'Très bel appartement');

-- --------------------------------------------------------

--
-- Structure de la table `messenger_messages`
--

DROP TABLE IF EXISTS `messenger_messages`;
CREATE TABLE IF NOT EXISTS `messenger_messages` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `headers` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue_name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `available_at` datetime NOT NULL,
  `delivered_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_75EA56E0FB7336F0` (`queue_name`),
  KEY `IDX_75EA56E0E3BD61CE` (`available_at`),
  KEY `IDX_75EA56E016BA31DB` (`delivered_at`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `messenger_messages`
--

INSERT INTO `messenger_messages` (`id`, `body`, `headers`, `queue_name`, `created_at`, `available_at`, `delivered_at`) VALUES
(1, 'O:36:\\\"Symfony\\\\Component\\\\Messenger\\\\Envelope\\\":2:{s:44:\\\"\\0Symfony\\\\Component\\\\Messenger\\\\Envelope\\0stamps\\\";a:1:{s:46:\\\"Symfony\\\\Component\\\\Messenger\\\\Stamp\\\\BusNameStamp\\\";a:1:{i:0;O:46:\\\"Symfony\\\\Component\\\\Messenger\\\\Stamp\\\\BusNameStamp\\\":1:{s:55:\\\"\\0Symfony\\\\Component\\\\Messenger\\\\Stamp\\\\BusNameStamp\\0busName\\\";s:21:\\\"messenger.bus.default\\\";}}}s:45:\\\"\\0Symfony\\\\Component\\\\Messenger\\\\Envelope\\0message\\\";O:51:\\\"Symfony\\\\Component\\\\Mailer\\\\Messenger\\\\SendEmailMessage\\\":2:{s:60:\\\"\\0Symfony\\\\Component\\\\Mailer\\\\Messenger\\\\SendEmailMessage\\0message\\\";O:28:\\\"Symfony\\\\Component\\\\Mime\\\\Email\\\":6:{i:0;N;i:1;N;i:2;s:108:\\\"<p>Parrain: bardella (bardou@yahoo.com, 0612345678)</p><p>Filleul: Lescaut (lesc@cramail.fr, 0765243267)</p>\\\";i:3;s:5:\\\"utf-8\\\";i:4;a:0:{}i:5;a:2:{i:0;O:37:\\\"Symfony\\\\Component\\\\Mime\\\\Header\\\\Headers\\\":2:{s:46:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\Headers\\0headers\\\";a:3:{s:4:\\\"from\\\";a:1:{i:0;O:47:\\\"Symfony\\\\Component\\\\Mime\\\\Header\\\\MailboxListHeader\\\":5:{s:50:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\AbstractHeader\\0name\\\";s:4:\\\"From\\\";s:56:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\AbstractHeader\\0lineLength\\\";i:76;s:50:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\AbstractHeader\\0lang\\\";N;s:53:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\AbstractHeader\\0charset\\\";s:5:\\\"utf-8\\\";s:58:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\MailboxListHeader\\0addresses\\\";a:1:{i:0;O:30:\\\"Symfony\\\\Component\\\\Mime\\\\Address\\\":2:{s:39:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Address\\0address\\\";s:16:\\\"bardou@yahoo.com\\\";s:36:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Address\\0name\\\";s:0:\\\"\\\";}}}}s:2:\\\"to\\\";a:1:{i:0;O:47:\\\"Symfony\\\\Component\\\\Mime\\\\Header\\\\MailboxListHeader\\\":5:{s:50:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\AbstractHeader\\0name\\\";s:2:\\\"To\\\";s:56:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\AbstractHeader\\0lineLength\\\";i:76;s:50:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\AbstractHeader\\0lang\\\";N;s:53:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\AbstractHeader\\0charset\\\";s:5:\\\"utf-8\\\";s:58:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\MailboxListHeader\\0addresses\\\";a:1:{i:0;O:30:\\\"Symfony\\\\Component\\\\Mime\\\\Address\\\":2:{s:39:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Address\\0address\\\";s:28:\\\"f.roblot.coulanges@gmail.com\\\";s:36:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Address\\0name\\\";s:0:\\\"\\\";}}}}s:7:\\\"subject\\\";a:1:{i:0;O:48:\\\"Symfony\\\\Component\\\\Mime\\\\Header\\\\UnstructuredHeader\\\":5:{s:50:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\AbstractHeader\\0name\\\";s:7:\\\"Subject\\\";s:56:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\AbstractHeader\\0lineLength\\\";i:76;s:50:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\AbstractHeader\\0lang\\\";N;s:53:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\AbstractHeader\\0charset\\\";s:5:\\\"utf-8\\\";s:55:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\UnstructuredHeader\\0value\\\";s:31:\\\"Mail automatique: Ajout Parrain\\\";}}}s:49:\\\"\\0Symfony\\\\Component\\\\Mime\\\\Header\\\\Headers\\0lineLength\\\";i:76;}i:1;N;}}s:61:\\\"\\0Symfony\\\\Component\\\\Mailer\\\\Messenger\\\\SendEmailMessage\\0envelope\\\";N;}}', '[]', 'default', '2024-05-20 11:36:42', '2024-05-20 11:36:42', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `parrainage`
--

DROP TABLE IF EXISTS `parrainage`;
CREATE TABLE IF NOT EXISTS `parrainage` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nom_parrain` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail_parrain` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel_parrain` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_filleul` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail_filleul` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel_filleul` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `parrainage`
--

INSERT INTO `parrainage` (`id`, `nom_parrain`, `mail_parrain`, `tel_parrain`, `nom_filleul`, `mail_filleul`, `tel_filleul`) VALUES
(3, 'ana', 'analov@hotmail.com', '0645342376', 'julie', 'julie@guli.fr', '0614672345'),
(6, 'jordan', 'jordi@hotmail.fr', '0642987867', 'gisele', 'gigi@yahoo.fr', '0712673578'),
(8, 'raza', 'speedyfam74@gamil.com', '0618000024', 'FRANCKI', 'FRAZAKANAIVO@CREDITETFINANCE.fr', '065455889EZDEFEF'),
(11, 'Doe', 'jd@hotmail.fr', '0678287863', 'gouly', 'gouly@gmail.com', '0617283746'),
(13, 'edouard', 'ed@yahoo.fr', '01328934565', 'philippe', 'phil@gmail.com', '0378654987'),
(17, 'bardella', 'bardou@yahoo.com', '0612345678', 'Lescaut', 'lesc@cramail.fr', '0765243267'),
(19, 'Gomes', 'm.gom@hotmail.com', '0675638798', 'julien', 'ju@gmail.com', '0712345699');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`) VALUES
(1, 'Frazakanaivo@creditetfinance.fr', '[]', '$2y$13$K0/moZdHovJpm8xLj1K9juShCuvm8HkUZUHRtd5RwvV.VspWYYLK2');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
