<?php

namespace App\Repository;

use App\Entity\Habitation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Habitation>
 *
 * @method Habitation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Habitation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Habitation[]    findAll()
 * @method Habitation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HabitationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Habitation::class);
    }

    //    /**
    //     * @return Habitation[] Returns an array of Habitation objects
    //     */
       public function findByExampleField($value): array
       {
           return $this->createQueryBuilder('h')
               ->andWhere('h.exampleField = :val')
               ->setParameter('val', $value)
               ->orderBy('h.id', 'DESC')
               ->setMaxResults(10)
               ->getQuery()
               ->getResult()
           ;
       }

       public function findOneBySomeField($value): ?Habitation
       {
           return $this->createQueryBuilder('h')
               ->andWhere('h.exampleField = :val')
               ->setParameter('val', $value)
               ->getQuery()
               ->getOneOrNullResult()
           ;
       }
     // Méthode pour rechercher des habitations en fonction des critères spécifiés
     public function findByCriteria($criteria)
     {
         $qb = $this->createQueryBuilder('h');
 
         if (!empty($criteria['departement'])) {
             $qb->andWhere('h.departement = :departement')
                 ->setParameter('departement', $criteria['departement']);
         }

         if (!empty($criteria['type'])) {
            $qb->andWhere('h.type = :type')
                ->setParameter('type', $criteria['type']);
        }

        if (!empty($criteria['surface_min'])) {// valeur qu'on rentre dans le formulaire = celle de rechercheType
            $qb->andWhere('h.surface >= :surface_min')//1ere valeur = celle de habitationType; 2eme valeur = rechercheType
                ->setParameter('surface_min', $criteria['surface_min']);// les 2 valeurs sont celles de rechercheType
        }

        // if (!empty($criteria['prix_min'])) {
        //     $qb->andWhere('h.prix_min >= :prix_min')//définir le prix min à supérieur ou égal
        //         ->setParameter('prix_min', $criteria['prix_min']);
        // }
        if (!empty($criteria['prix_max'])) {
            $qb->andWhere('h.prix <= :prix_max')// définir le prix max à inférieur ou égal
                ->setParameter('prix_max', $criteria['prix_max']);
        }

        if (!empty($criteria['loyer_min'])) {
            $qb->andWhere('h.loyer >= :loyer_min')//définir le loyer min à supérieur ou égal
                ->setParameter('loyer_min', $criteria['loyer_min']);
        }

        // if (!empty($criteria['loyerMax'])) {
        //     $qb->andWhere('h.loyer <= :max_loyer')//définir le loyer max à inférieur ou égal
        //         ->setParameter('max_loyer', $criteria['loyer']);
        // }

        if (!empty($criteria['rentabilite'])) {
            $qb->andWhere('h.rentabilite >= :rentabilite')//définir la rentabilité minimum
                ->setParameter('rentabilite', $criteria['rentabilite']);
        }

 
         // Ajoutez d'autres critères selon vos besoins
 
         return $qb->getQuery()->getResult();
     }
}
