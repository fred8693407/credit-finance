<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\LessThan;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;

class CalculCreditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('montant', NumberType::class, [
            'attr' => [
                'class' => 'form-control',
                'placeholder' => '€',
            ],
            'label' => 'Montant d\'achat',
            'constraints' => [
                new NotBlank(),
                new GreaterThanOrEqual([
                    'value' => 0,
                    'message' => 'Le montant doit être supérieur ou égal à zéro.',
                ]),
                new LessThan([
                    'value' => 10000000, // 10 millions
                    'message' => 'Veuillez entrer un montant cohérent svp !',
                ]),
            ],
        ])
        ->add('taux', NumberType::class, [ 'attr'=>['class'=>'form-control'],
            'label' => 'Taux d\'intérêt annuel (%)',
            'constraints' => [
                new NotBlank(),
                new GreaterThanOrEqual(0),
                new LessThan([
                    'value' => 50, // 50 % max
                    'message' => 'Veuillez entrer un taux cohérent svp !',
                ]),
            ],
        ])
        ->add('duree', NumberType::class, [ 'attr'=>['class'=>'form-control'],
            'label' => 'Durée en mois',
            'constraints' => [
                new NotBlank(),
                new GreaterThanOrEqual(1),
                new LessThan([
                    'value' => 360, // 360 mois  = 30 ans
                    'message' => 'Veuillez entrer des annuités cohérentes svp !',
                ]),
            ],
        ])
        ->add('calculer', SubmitType::class, [
            'attr' => ['class' => 'btn bouton mt-3 mb-5 btn-reduit'],
            'label' => 'Calculer les mensualités',
        ]);
        
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
