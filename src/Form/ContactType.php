<?php

namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', TextType::class, ['attr'=> [
                                                    "class" => "form-control mb-2",
                                                    "label"=> "Nom",
                                                    'oninput' => 'this.value=this.value.replace(/[^a-zA-Z\s-]/g,"");', // Empêche l'entrée de ponctuation et de chiffres sauf le tiret
                                                    ]])

            ->add('prenom', TextType::class, ['attr'=> [
                                                    "class" => "form-control mb-2",
                                                    "label"=> "Prénom *",
                                                    'oninput' => 'this.value=this.value.replace(/[^a-zA-Z\s-]/g,"");', // Empêche l'entrée de ponctuation et de chiffres sauf le tiret
                                                    ]])
            ->add('email', EmailType::class, ['attr'=> ["class" => "form-control mb-2",
                                              "label"=> "E-mail", "placeholder"=>"ex : votre_nom@yahoo.fr"]])
            ->add('tel', TextType::class, [ 'attr' => [
                                                    "class" => "form-control mb-2",
                                                    "label" => "N° mobile ou fixe",
                                                    "placeholder" => "06XXXXXXXX",
                                                    'maxlength' => 10, // Limite à 10 caractères
                                                    'oninput' => 'this.value=this.value.replace(/[^0-9]/g,"");', // Interdire l'entrée de lettres
                                                ],
                                                'constraints' => [
                                                    new Length(['min' => 10, 'max' => 10]), // Exige 10 caractères
                                                    new Regex([
                                                        'pattern' => '/^0[1-9]\d{8}$/', // Doit commencer par 0 et contenir 10 chiffres au total
                                                        'message' => 'Le numéro de téléphone doit contenir 10 chiffres et commencer par 0.',
                                                    ]),
                                                ],
                                            ])
                                               

            ->add('vos_demandes', TextareaType::class, ['attr'=>['class'=>'form-control mb-2',
                                                        'maxlength'=> '255',
                                                        'mimeTypesMessage'=> "nombre de caractères Max : 255",
                                                        'oninput' => 'this.value=this.value.replace(/[^a-zA-Z\s-\']/g,"");' // autorise seulement l'entrée de lettres et le tiret
                                                        ],
                                                        'label'=> 'Vos demandes',
                                                        'required' => false,
                                                        ])
            ->add('disponibilites', null, ['attr'=> ["class"=> 'form-control'],
            'widget' => 'single_text',
            'label'=> 'Date et heure auxquelles vous souhaitez qu\'on vous contacte *',
            'required' => true,
    
        ])
        ->add("creer" , SubmitType::class , ["label"=> isset($options["label"]) ? $options["label"]  : "Me rappeler" , "attr" => [ "class" => "btn bouton col-6 mt-2 px-3 responsive-btn-modif" ]])
        ;
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}
