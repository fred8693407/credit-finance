<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\LessThan;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;

class ReducImpotType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('investissement', NumberType::class, [
                'label' => 'Montant de l\'investissement',
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new GreaterThanOrEqual([
                        'value' => 50000,
                        'message' => 'Le montant doit être supérieur ou égal à 50 000 €.',
                    ]),
                    new LessThan([
                        'value' => 10000000, // 10 millions
                        'message' => 'Veuillez entrer un montant cohérent svp !',
                    ]),
                ],
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'exemple : 100000 €',
                    'oninput' => 'this.value = this.value.replace(/[^0-9]/g,"");'
                ],
            ])
            
            ->add('calculer', SubmitType::class, [
                'attr' => ['class' => 'btn bouton mt-3 mb-5 btn-reduit']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
