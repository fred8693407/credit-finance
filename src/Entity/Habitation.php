<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\HabitationRepository;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints\NotBlank;

#[ORM\Entity(repositoryClass: HabitationRepository::class)]
class Habitation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $ville = null;

    #[ORM\Column(length: 255)]
    private ?string $adresse = null;

    #[ORM\Column]
    private ?int $code_postal = null;

    #[ORM\Column(length: 50)]
    private ?string $type = null;

    #[ORM\Column]
    private ?int $surface = null;

    // #[ORM\Column]
    // private ?int $surfaceMin = null;

    #[ORM\Column]
    private ?int $prix = null;
    
    // #[ORM\Column]
    // private ?int $prixMax = null;

    #[ORM\Column]
    private ?int $loyer = null;
    
    // #[ORM\Column]
    // private ?int $loyerMin = null;

    #[ORM\Column]
    private ?int $rentabilite = null;
    
    // #[ORM\Column]
    // private ?int $rentabiliteMin = null;

    #[ORM\Column(type: "string", length: 255)]
    private string $image;

    // #[ORM\Column(length: 255)]
    // private ?string $image = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $date_creation = null;

    #[ORM\Column(length: 100)]
    private ?string $departement = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $description = null;

    // #[ORM\Column]
    // private ?int $numero_departement = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): static
    {
        $this->ville = $ville;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): static
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCodePostal(): ?int
    {
        return $this->code_postal;
    }

    public function setCodePostal(int $code_postal): static
    {
        $this->code_postal = $code_postal;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getSurface(): ?int
    {
        return $this->surface;
    }

    public function setSurface(int $surface): static
    {
        $this->surface = $surface;

        return $this;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): static
    {
        $this->prix = $prix;

        return $this;
    }

    public function getLoyer(): ?int
    {
        return $this->loyer;
    }

    public function setLoyer(int $loyer): static
    {
        $this->loyer = $loyer;

        return $this;
    }

    public function getRentabilite(): ?int
    {
        return $this->rentabilite;
    }

    public function setRentabilite(int $rentabilite): static
    {
        $this->rentabilite = $rentabilite;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): static
    {
        $this->image = $image;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->date_creation;
    }

    public function setDateCreation(\DateTimeInterface $date_creation): static
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    public function getDepartement(): ?string
    {
        return $this->departement;
    }

    public function setDepartement(string $departement): static
    {
        $this->departement = $departement;

        return $this;
    }



    // public function getNumeroDepartement(): ?int
    // {
    //     return $this->numero_departement;
    // }

    // public function setNumeroDepartement(int $numero_departement): static
    // {
    //     $this->numero_departement = $numero_departement;

    //     return $this;
    // }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of surface_min
     */ 
    // public function getSurfaceMin()
    // {
    //     return $this->surfaceMin;
    // }

    /**
     * Set the value of surface_min
     *
     * @return  self
     */ 
    // public function setSurfaceMin($surfaceMin)
    // {
    //     $this->surfaceMin = $surfaceMin;

    //     return $this;
    // }

    /**
     * Get the value of prixMax
     */ 
    // public function getPrixMax()
    // {
    //     return $this->prixMax;
    // }

    /**
     * Set the value of prixMax
     *
     * @return  self
     */ 
    // public function setPrixMax($prixMax)
    // {
    //     $this->prixMax = $prixMax;

    //     return $this;
    // }

    /**
     * Get the value of loyerMin
     */ 
    // public function getLoyerMin()
    // {
    //     return $this->loyerMin;
    // }

    /**
     * Set the value of loyerMin
     *
     * @return  self
     */ 
    // public function setLoyerMin($loyerMin)
    // {
    //     $this->loyerMin = $loyerMin;

    //     return $this;
    // }

    /**
     * Get the value of rentabiliteMin
     */ 
    // public function getRentabiliteMin()
    // {
    //     return $this->rentabiliteMin;
    // }

    /**
     * Set the value of rentabiliteMin
     *
     * @return  self
     */ 
    // public function setRentabiliteMin($rentabiliteMin)
    // {
    //     $this->rentabiliteMin = $rentabiliteMin;

    //     return $this;
    // }
}
