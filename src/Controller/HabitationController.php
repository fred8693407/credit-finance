<?php

namespace App\Controller;

use App\Model\SearchData;
use App\Entity\Habitation;
use App\Form\HabitationType;
use App\Entity\FormulaireRecherche;
use Doctrine\ORM\Query\Expr\OrderBy;
use App\Form\FormulaireRechercheType;
use App\Repository\HabitationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class HabitationController extends AbstractController
{
    #[Route("/parc_immobilier", name:"parc_immobilier")]
    
    public function rp(HabitationRepository $hr, Request $request) : Response {
      
      $form = $this->createForm(FormulaireRechercheType::class);//on spécifie quel formulaire on utilise

      $form->handleRequest($request);//on récupère la requête sécurisée
      //si le formulaire est soumis et qu'il est valide, on peut y accéder
      if ($form->isSubmitted() && $form->isValid()) {  
        $criteria = $form->getData();
        $resultats = $hr->findByCriteria($criteria);//on recupere les resultats de recherche
        
          // Gérer la soumission du formulaire
          // Par exemple, redirection vers une autre page avec les paramètres de recherche
          return $this->render('recherche/resultatRecherche.html.twig', ['resultats'=>$resultats, 'form'=>$form->createView()]);//on retourne les données vers la vue
      }

      // On récupère les logements de type résidence principale et on les affiche par ordre décroissant, le dernier bien rentré sera affiché en premier
      $habitation = $hr->findBy([], ['id' => 'DESC']); 
      return $this->render("habitation/parc_immobilier.html.twig", ["habitation" => $habitation, 'form'=>$form->createView()]);
        }
        
// *************************************************************************************************
// ************GESTION**************************
    #[Route("/gestion", name: "gestion")]
    public function gestionHabitation( HabitationRepository $habitationRepository) : Response{
    $habitation = $habitationRepository->findAll() ;
    return $this->render("habitation/gestionHabitation.html.twig", ["habitation" => $habitation]);
    }

    // ***ajouter***********
    #[Route("/ajout_habitation", name: "ajout_habitation")]
    public function ajouter(Request $request, EntityManagerInterface $em)
    {
        $habitation = new Habitation();
        $form = $this->createForm(HabitationType::class, $habitation);//on va utiliser le formulaire de type HabitationType
        
        $form->handleRequest($request);
    
        if ($form->isSubmitted() && $form->isValid()) {
            // Gestion de l'upload d'image
            /** @var UploadedFile $imageFile */
            $imageFile = $form->get('image')->getData();
    
            if ($imageFile) {
                $originalFilename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
                
                // Génération d'un nom de fichier unique
                $newFilename = $originalFilename.'-'.uniqid().'.'.$imageFile->guessExtension();
    
                // Déplacement de l'image dans le répertoire de destination
                try {
                    $imageFile->move(
                        $this->getParameter('uploads_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // Gérer les erreurs d'upload
                }
    
                // Stocker le nom de fichier dans l'entité Habitation
                $habitation->setImage($newFilename);
            }
    
            // Persistez et flush l'entité
            $em->persist($habitation);
            $em->flush();
    
            // Rediriger vers la même page après l'ajout
            return $this->redirectToRoute("ajout_habitation");
        }
    
        // Affichage du formulaire dans le template
        return $this->render("habitation/ajout_habitation.html.twig", [
            "form" => $form->createView()
        ]);
      }


    //  MODIFIER *********************************************
    #[Route("/modifier/{id}", name: "modifier")]
public function modifierHabitation(int $id, HabitationRepository $habitationRepository, Request $request, EntityManagerInterface $em)
{
    $habitation = $habitationRepository->find($id);

    if (!$habitation) {
        throw $this->createNotFoundException('Aucune habitation trouvée pour l\'id '.$id);
    }

    // Récupérer le nom de fichier actuel de l'image
    $imageFilename = $habitation->getImage();

    // Créer un formulaire en passant le nom de fichier actuel comme option
    $form = $this->createForm(HabitationType::class, $habitation, [
        'label' => 'modifier',
        'image_filename' => $imageFilename // Passer le nom de fichier actuel comme option
    ]);

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
        // Gérer l'upload d'une nouvelle image
        $newImageFile = $form->get('image')->getData();

        if ($newImageFile) {
            $originalFilename = pathinfo($newImageFile->getClientOriginalName(), PATHINFO_FILENAME);
            $newFilename = $originalFilename.'-'.uniqid().'.'.$newImageFile->guessExtension();

            try {
                $newImageFile->move(
                    $this->getParameter('uploads_directory'),
                    $newFilename
                );
            } catch (FileException $e) {
                // Gérer les erreurs d'upload
            }

            // Mettre à jour le nom de fichier de l'image dans l'entité
            $habitation->setImage($newFilename);

            // Supprimer l'ancienne image si nécessaire
            if ($imageFilename) {
                unlink($this->getParameter('uploads_directory').'/'.$imageFilename);
            }
        }

        $em->flush();

        return $this->redirectToRoute("gestion");
    }

    return $this->render("habitation/modifierHabitation.html.twig", [
        "form" => $form->createView(),
    ]);
}

    

    // **SUPPRIMER****************************************
    #[Route("/supprimer/{id}",name:"supprimer")]
    public function suppressionHabitation (EntityManagerInterface $em, HabitationRepository $hr, int $id):Response{
    $habitation = $hr->findOneBy(["id" => $id]);
    $em->remove($habitation);
    $em->flush();
    return $this->redirectToRoute("gestion");//correspond au path du menu header

    }
}
