<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Repository\ContactRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContactController extends AbstractController {

  #[Route("/contact", name: "contact")]
  public function contact(Request $request, EntityManagerInterface $entityManagerInterface): Response {
    $contact = new Contact();
    $form = $this->createForm(ContactType::class, $contact);

    $form->handleRequest($request); // gère automatiquement les requêtes POST, ce qui réduit les risques d'erreurs de traitement manuel des données.

    //Via symfony form -> Protection CSRF(action et contenu malveillant) : Symfony Forms intègre par défaut une protection contre les attaques CSRF (Cross-Site Request Forgery). Cela empêche un attaquant de soumettre un formulaire au nom de l'utilisateur sans son consentement.

    if ($form->isSubmitted() && $form->isValid()) {
      $entityManagerInterface->persist($contact);
      $entityManagerInterface->flush();

      // Ajouter un message flash pour indiquer que le formulaire a été soumis avec succès
      $this->addFlash('success', 'Votre demande a bien été envoyée. Nous revenons vers vous dans les plus brefs délais !');

      return $this->redirectToRoute("parrainage");
    }

    return $this->render("front/contact.html.twig", ['form' => $form->createView()]);
  }

  #[Route('/gestion-contact', name:'gestion-contact')]
  public function gestionContact(ContactRepository $contactRepository): Response {
    $contacts = $contactRepository->findAll();
    return $this->render("gestion-contact/gestion-contact.html.twig", ['contacts' => $contacts]);
  }

  #[Route("/supprimer/{id}", name: "supprimer")]
  public function suppressionContact(EntityManagerInterface $em, ContactRepository $cr, int $id): Response {
    $contact = $cr->findOneBy(["id" => $id]);

    if (!$contact) {
      // Contact non trouvé, ajouter un message flash et rediriger
      $this->addFlash('error', 'Contact non trouvé.');
      return $this->redirectToRoute("gestion-contact");
    }

    $em->remove($contact);
    $em->flush();

    // Ajouter un message flash pour indiquer que le contact a été supprimé
    $this->addFlash('success', 'Contact supprimé avec succès.');

    return $this->redirectToRoute("gestion-contact");
  }

}
