<?php

namespace App\Controller;

use App\Form\ReducImpotType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CreditController extends AbstractController{

  #[Route('/credit', name: 'credit')]
  public function credit(Request $request)
  {

      $form = $this->createForm(ReducImpotType::class);
      $form->handleRequest($request);

         // Initialiser les variables par défaut
         $investissement = null;
         $premierReduction = null;
         $deuxiemeReduction = null;
         $troisiemeReduction = null;

      if ( $form->isSubmitted() && $form->isValid() ){
        $data = $form->getData();
        $investissement = (float) $data['investissement'];

        $premierReduction = $investissement * (12 / 100);
        $deuxiemeReduction = $investissement * (18 / 100);
        $troisiemeReduction = $investissement * (21 / 100);
      }


      // Par défaut, l'investissement est null
     

      return $this->render("front/credit.html.twig", [
          'investissement' => $investissement,
          'premierReduction' => $premierReduction,
          'deuxiemeReduction' => $deuxiemeReduction,
          'troisiemeReduction' => $troisiemeReduction,
          'form' => $form->createView(),
      ]);
  }
  
  }
