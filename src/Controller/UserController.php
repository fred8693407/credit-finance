<?php

namespace App\Controller;

use App\Form\RegistrationFormType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[Route('/user')]
#[IsGranted('ROLE_USER')]
class UserController extends AbstractController
{
    #[Route('/', name: 'user')]
    public function index_user(UserRepository $userRepo): Response
    {
        $user = $userRepo->findAll();
        return $this->render('user/index.html.twig', [
            'user' => $user
        ]);
    }

    #[Route('/update/{id}', name: "update_user")]
    public function update_user(int $id, UserRepository $userRepo, UserPasswordHasherInterface $userPasswordHasher, Request $request, EntityManagerInterface $em){
        $user = $userRepo->findOneBy(['id' => $id]);

        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
                );
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('user');
        }
        return $this->render('user/update.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
