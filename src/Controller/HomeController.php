<?php
namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Repository\LogementsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController{

  #[Route('/', name: 'home')]
  public function index() {
    return $this->render('front/home.html.twig');
  }

  #[Route("/investissement_locatif", name:"investissement_locatif")]
  public function investissement() {
    return $this->render("front/investissement.html.twig");
  }

  // #[Route("/residence_principale", name:"residence_principale")]
  // public function rp(LogementsRepository $logementsRepository) : Response {
  //   // On récupère les logements de type résidence principale
  //   $logements = $logementsRepository->findAll();
  //   return $this->render("front/residence_principale.html.twig", ["logements" => $logements]);
  // }

  #[Route("/placements", name:"placements")]
  public function placements(){
    return $this->render("front/placements.html.twig");
  }

  // #[Route("/contact", name: "contact")]
  // public function contact(Request $request, EntityManagerInterface $entityManagerInterface) {

  //   $contact = new Contact();
  //   $form = $this->createForm(ContactType::class, $contact);

  //   $form->handleRequest($request);

  //   if($form->isSubmitted() && $form->isValid()) {
  //     $entityManagerInterface->persist($contact);
  //     $entityManagerInterface->flush();

  //     return $this->redirectToRoute("parrainage");
      
  //   }

  //   return $this->render("front/contact.html.twig", ['form' => $form->createView()]);

  // }

  #[Route("/cgv-cgu", name: "cgv-cgu")]
  public function cgvCgu() {
    return $this->render("front/cgv-cgu.html.twig");
  }

}