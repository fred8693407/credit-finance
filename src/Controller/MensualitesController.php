<?php

namespace App\Controller;

use App\Form\CalculCreditType;
use App\Form\MensualitesFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MensualitesController extends AbstractController{
  
  #[Route('/mensualites', name: 'mensualites')]
  public function calculer(Request $request)
  {
      // Créer le formulaire
      $form = $this->createForm(CalculCreditType::class);//Appeller le formulaire auquel on fait référence

      // Traiter le formulaire soumis
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
          // Obtenir les données du formulaire
          $data = $form->getData();

          // Calculer les mensualités
          $tauxInteretMensuel = $data['taux'] / 12 / 100;
          $paiementMensuel = ($data['montant'] * $tauxInteretMensuel) / (1 - pow(1 + $tauxInteretMensuel, -$data['duree']));
          $montantTotalRembourse = $paiementMensuel * $data['duree'];

          // Rendre le template avec les résultats
          return $this->render('front/mensualites.html.twig', [
              'paiementMensuel' => round($paiementMensuel, 2),
              'montantTotalRembourse' => round($montantTotalRembourse, 2),
              'form' => $form->createView(),
          ]);
      }

      // Rendre le template avec le formulaire
      return $this->render('front/mensualites.html.twig', [
          'form' => $form->createView(),
      ]);
  }
}
