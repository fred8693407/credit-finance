<?php

namespace App\Controller;

use App\Entity\Parrainage;
use App\Form\ParrainageType;
use Symfony\Component\Mime\Email;
use App\Repository\ParrainageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;

class ParrainageController extends AbstractController
{
    #[Route('/parrainage', name: 'parrainage')]
    public function new_parrainage(Request $request, EntityManagerInterface $em, MailerInterface $mailer): Response
    {
        $parrainage = new Parrainage();
        $form = $this->createForm(ParrainageType::class, $parrainage);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em->persist($parrainage);
            $em->flush();
            $this->addFlash('success', 'Le formulaire a été envoyé avec succès !');

             // Redirection pour afficher un formulaire vierge
             return $this->redirectToRoute('parrainage');

             // Récupérer l'email de l'expéditeur depuis le formulaire
            //  $expediteurEmail = $parrainage->getMailParrain();
            
            // Création de l'email
            // $email = (new Email())
            //     ->from($expediteurEmail)
            //     ->to('danilov.a@hotmail.fr')
            //     ->subject('Mail automatique: Ajout Parrain')
            //     ->html(sprintf(
            //         '<p>Parrain: %s (%s, %s)</p><p>Filleul: %s (%s, %s)</p>',
            //         $parrainage->getNomParrain(),
            //         $parrainage->getMailParrain(),
            //         $parrainage->getTelParrain(),
            //         $parrainage->getNomFilleul(),
            //         $parrainage->getMailFilleul(),
            //         $parrainage->getTelFilleul()
            //     ));

            // Envoi de l'email
            // $mailer->send($email);

        }

        return $this->render('parrainage/index.html.twig', [ "form" => $form->createView() ]);
    }

    #[IsGranted('ROLE_USER')]
    #[Route('/listeParrains', name: 'liste_parrains')]
    public function liste_parrainage(ParrainageRepository $parrainageRepo): Response {
        $parrains = $parrainageRepo -> findAll();
        return $this->render('parrainage/listeParrains.html.twig', [ 'parrains' => $parrains ]);
    }

    #[Route('/update/{id}', name: 'update_parrainage')]
    public function update_parrainage(int $id, ParrainageRepository $parrainageRepo, Request $request, EntityManagerInterface $em){
        $parrainage = $parrainageRepo->findOneBy([ 'id' => $id]);

        $form = $this->createForm(ParrainageType::class, $parrainage, ['label' => 'Modifier']);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($parrainage);
            $em->flush();
            return $this->redirectToRoute("liste_parrains");
        }
        return $this->render('parrainage/update.html.twig', ['form' => $form->createView()]);
    }

    #[Route('/delete/{id}', name: 'delete_parrainage')]
    public function delete_parrainage(int $id, ParrainageRepository $parrainageRepo, EntityManagerInterface $em){
        $parrainage = $parrainageRepo->findOneBy(['id' => $id]);
        $em->remove($parrainage);
        $em->flush();
        return $this->redirectToRoute('liste_parrains');
    }
}
