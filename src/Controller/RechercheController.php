<?php
namespace App\Controller;

use App\Entity\Habitation;
use App\Form\FormulaireRechercheType;
use App\Repository\HabitationRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RechercheController extends AbstractController
{
    #[Route("/rechercher", name: "rechercher")]
    public function rechercher(HabitationRepository $hr, Request $request): Response//on utilise la methode repository pour afficher les résulats
{
  
    $form = $this->createForm(FormulaireRechercheType::class);//on spécifie le formulaire utilisé
    $form->handleRequest($request);

    $resultat = [];// on initialise le tableau des résultats

    // Si le formulaire a été soumis et qu'il est valide
    if ($form->isSubmitted() && $form->isValid()) {

        if ($form->get('reinitialiser')->isClicked()) {
            return $this->redirectToRoute('rechercher');
        }


        $criteria = $form->getData();
        $resultats = $hr->findByCriteria($criteria);

        return $this->render('recherche/resultatRecherche.html.twig', [
            'resultats'=>$resultats, 'form'=>$form->createView()] // Transmettez la variable 'resultats' à la vue
        );
  
    }

    // Afficher tous les logements si aucun critère n'est spécifié
    $resultats = $hr->findAll();

    return $this->render('habitation/parc_immobilier.html.twig', [
        'form' => $form->createView(),
        ['resultat' => $resultat, 'form'=>$form->createView()] // Assurez-vous de toujours transmettre 'resultats' à la vue
    ]);
}

}
